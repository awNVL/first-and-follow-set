/* Hash map - header */
#ifndef HASHMAP_H
#define HASHMAP_H
#include <iostream>
#include <fstream>
#include <list>
#include <string>
#include <vector>
#include <iterator>
#include <cstdlib>
using std::cout;
using std::endl;
using std::list;
using std::string;
using std::vector;
using std::ifstream;
using std::ofstream;

class Hashmap{
	public:
		Hashmap();		//default constructor
		Hashmap(const Hashmap& another);		//copy constructor
		Hashmap& operator=(const Hashmap& another);		//assignment operator
		~Hashmap();		//destructor
		
		void insertRecord(const string& str);		//function to insert a record to hash table
		bool findRecord(const string& str);		//function to find a record in hash table
		void output(ofstream& outfile,char seperator);		//output all the record of hash table to file
		int size();		//function to get the number of hash record
		
		friend class Nonterminal;
		
	protected:
		int hash(const string& str);		//function to get hash value from a string
		int itemNumber;		//number of hash record
		vector< list<string> > hashTable;
};
#endif
