/* Nonterminal - header */
#ifndef NONTERMINAL_H
#define NONTERMINAL_H
#include "Hashmap.h"

class Nonterminal{
	public:
		Nonterminal();		//default constructor
		Nonterminal(int num,string str);		//constructor
		Nonterminal(const Nonterminal& another);		//copy constructor
		Nonterminal& operator=(const Nonterminal& another);		//assignment operator
		~Nonterminal();		//destructor
		
		void addToProductionSet(const string& str);		//function to add production to production set
		void constructFirst();
		void initializeFollowSet();
		void constructFollow();
		void writeToFile(ofstream& outfile);		//write infomation to file
		const string& getName();		//get name
		void setIndex(int num);		//mutator to set nonterminal's index
		void setName(string str);		//mutator to set nonterminal's name
		void outProduction();		//output production set
		bool firstSetChanged();		//to indicate if first set changed
		bool proFollowSetChanged();		//to indicate if any symbol's follow set changed
		
	protected:
		void addToFirstSet(const string& str);		//function to add a terminal to first set
		void addToFollowSet(const string& str);		//function to add a terminal to follow set
		bool findSymbol(const string& symbol,Hashmap& hm);		//function to find if a symbol already in a hash map container
		
		int index;
		string name;
		bool firstChanged;		//once new symbol added into first set,first set changed
		bool proFollowChanged;		//at least a symbol's follow set changed,this symbol is in a production
		list<string> productionSet;
		Hashmap firstSet;
		Hashmap followSet;
};
#endif
