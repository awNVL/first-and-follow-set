/* Hash map - definition */
#include "Hashmap.h"
const int SIZE=107;

/* Default constructor */
Hashmap::Hashmap():hashTable(SIZE),itemNumber(0){
	for(int i=0;i<hashTable.size();i++)
		hashTable[i].resize(0);
}

/* Copy constructor */
Hashmap::Hashmap(const Hashmap& another){
	itemNumber=another.itemNumber;
	hashTable=another.hashTable;
}

/* Assignment operator */
Hashmap& Hashmap::operator=(const Hashmap& another){
	if(this==&another)
		return *this;
	itemNumber=another.itemNumber;
	hashTable=another.hashTable;
}

/* Destructor */
Hashmap::~Hashmap(){
}

/* Function to insert a record to hash table */
void Hashmap::insertRecord(const string& str){
	int h=hash(str);
	if(!findRecord(str)){
		++itemNumber;
		hashTable[h].push_back(str);
	}
}

/* Function to find a record in hash table */
bool Hashmap::findRecord(const string& str){
	int h=hash(str);
	list<string>::iterator itr;
	for(itr=hashTable[h].begin();itr!=hashTable[h].end();++itr)
		if(*itr==str)
			return true;
	return false;	
}

/* Output all the record of hash table to file */
void Hashmap::output(ofstream& outfile,char separator){
	vector< list<string> >::iterator vitr;
	list<string>::iterator litr;
	int count=0;
	for(vitr=hashTable.begin();vitr!=hashTable.end();++vitr){
		if((*vitr).size()!=0){
			for(litr=(*vitr).begin();litr!=(*vitr).end();++litr){
				++count;
				outfile<<*litr;
				if(count!=size())
					outfile<<separator;
			} 
		}
	}
}

/* Function to get the number of hash record */
int Hashmap::size(){
	return itemNumber;
}

/* Function to get hash value from a string */
int Hashmap::hash(const string& str){
	int value=0;
	for(int i=0;i<str.length();i++)
		value=((value<<4)+str[i])%SIZE;
	return value;
}
