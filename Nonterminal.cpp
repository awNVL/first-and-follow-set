/* Nonterminal - definition */
#include "Nonterminal.h"
extern vector<Nonterminal> nonterminal;

bool isTerminal(const string& symbol);
const Nonterminal& findNonterminal(const string& symbol);
Nonterminal& findChangeableNonterminal(const string& symbol);
string firstSymbol(const string& production);
void cutFirstSymbol(string& production);

/* Function to get a const nonterminal,take care the parameter should only be a nonterminal */
const Nonterminal& findNonterminal(const string& symbol){
	if(isTerminal(symbol)){
		cout<<"find nonterminal error"<<endl;
		exit(1);
	}
	vector<Nonterminal>::iterator itr;
	for(itr=nonterminal.begin();itr!=nonterminal.end();++itr)
		if((*itr).getName()==symbol)
			return *itr;
}

/* Function to get a changeable nonterminal,take care the parameter should only be a nonterminal */
Nonterminal& findChangeableNonterminal(const string& symbol){
	if(isTerminal(symbol)){
		cout<<"find nonterminal error"<<endl;
		exit(1);
	}
	vector<Nonterminal>::iterator itr;
	for(itr=nonterminal.begin();itr!=nonterminal.end();++itr)
		if((*itr).getName()==symbol)
			return *itr;
}

/* Default constructor */
Nonterminal::Nonterminal():index(0),name(),firstChanged(false),proFollowChanged(false),
                           productionSet(),firstSet(),followSet(){
}

/* Constructor */
Nonterminal::Nonterminal(int num,string str):index(num),name(str),firstChanged(false),proFollowChanged(false),
                                             productionSet(),firstSet(),followSet(){
}

/* Copy constructor */
Nonterminal::Nonterminal(const Nonterminal& another){
	index=another.index;
	name=another.name;
	firstChanged=another.firstChanged;
	proFollowChanged=another.proFollowChanged;
	productionSet=another.productionSet;
	firstSet=another.firstSet;
	followSet=another.followSet;
}

/* Assignment operator */
Nonterminal& Nonterminal::operator=(const Nonterminal& another){
	if(this==&another)
		return *this;
	index=another.index;
	name=another.name;
	firstChanged=another.firstChanged;
	proFollowChanged=another.proFollowChanged;
	productionSet=another.productionSet;
	firstSet=another.firstSet;
	followSet=another.followSet;
}

/* Destructor */
Nonterminal::~Nonterminal(){
}

/* Function to add a production to production set */
void Nonterminal::addToProductionSet(const string& str){
	productionSet.push_back(str);
}

/* Procedure to construct first set */
void Nonterminal::constructFirst(){
	list<string>::iterator pitr;
	firstChanged=false;		//if no new symbol added into first set after following operation,then first set will not change
	for(pitr=productionSet.begin();pitr!=productionSet.end();++pitr){
		string symbol=firstSymbol(*pitr);		//check each production's first symbol
		if(symbol==name){		//the first symbol is the nonterminal's name,check if it can be replaced by empty
			string empty("empty");
			if(findSymbol(empty,firstSet)){		//empty is in it's first set
				string proStr=*pitr;
				string space(" ");
				int start=proStr.find_first_of(space);
				start=proStr.find_first_not_of(space,start);
				symbol=proStr.substr(start,proStr.length());
			}
		}
		if(isTerminal(symbol)){
			if(!findSymbol(symbol,firstSet))		//new terminal gonna added into first set,so first set will change
				firstChanged=true;
			addToFirstSet(symbol);
		}
		else{
			const Nonterminal& rnon=findNonterminal(symbol);
			vector< list<string> >::const_iterator vitr;		//iterator should be const type as the const Nonterminal class
			for(vitr=rnon.firstSet.hashTable.begin();vitr!=rnon.firstSet.hashTable.end();++vitr){
				if((*vitr).size()!=0){
					list<string>::const_iterator litr;		//iterator should be const type as the const Nonterminal class
					for(litr=(*vitr).begin();litr!=(*vitr).end();++litr){
						if(!findSymbol(*litr,firstSet))		//new terminal gonna added into first set,so first set will change
							firstChanged=true;
						addToFirstSet(*litr);
					}
				}
			}
		}
	}
}

/* Function to add EOF to start symbol's follow set */
void Nonterminal::initializeFollowSet(){
	string eofStr("EOF");
	addToFollowSet(eofStr);
}

/* Procedure to construct follow set */
void Nonterminal::constructFollow(){
	list<string>::iterator pitr;
	proFollowChanged=false;		//if no symbol's follow set changed after following operation,then this indicator will not change
	for(pitr=productionSet.begin();pitr!=productionSet.end();++pitr){
		string proStr=*pitr;
		string symbol;
		string nextSymbol;
		string empty("empty");
		do{
			symbol=firstSymbol(proStr);
			cutFirstSymbol(proStr);
			if(!isTerminal(symbol)){		//if current symbol is nonterminal
				Nonterminal& rnon=findChangeableNonterminal(symbol);
				nextSymbol=firstSymbol(proStr);
				if(nextSymbol.length()!=0){		//if we have next symbol
					if(isTerminal(nextSymbol)){		//the next symbol is a terminal
						if(nextSymbol!=empty){
							if(!findSymbol(nextSymbol,rnon.followSet))		//one symbol's follow set will change
								proFollowChanged=true;
							rnon.addToFollowSet(nextSymbol);
						}
					}
					else{		//the next symbol is a nonterminal
						const Nonterminal& rnonNext=findNonterminal(nextSymbol);
						vector< list<string> >::const_iterator vitr;		//iterator should be const type as the const Nonterminal class
						for(vitr=rnonNext.firstSet.hashTable.begin();vitr!=rnonNext.firstSet.hashTable.end();++vitr){
							if((*vitr).size()!=0){
								list<string>::const_iterator litr;		//iterator should be const type as the const Nonterminal class
								for(litr=(*vitr).begin();litr!=(*vitr).end();++litr){
									if((*litr)!=empty){
										if(!findSymbol(*litr,rnon.followSet))		//one symbol's follow set will change
											proFollowChanged=true;
										rnon.addToFollowSet(*litr);
									}
								}
							}
						}
						/* Check if empty in the following symbol's first set */
						string tempProStr(proStr);
						string tempNextSymbol(nextSymbol);
						bool allEmpty=true;		//if all of the following symbol's first set have empty,this indicator will not change
						while(tempProStr.length()){
  						tempNextSymbol=firstSymbol(tempProStr);
  						cutFirstSymbol(tempProStr);
  						if(isTerminal(tempNextSymbol)){
  							if(tempNextSymbol==empty)		
  								continue;
  							else{
  								allEmpty=false;		//a non-empty terminal,so indicator will change
  								break;
  							}
  						}
  						/* Take care don't find nonterminal for terminal*/
  						Nonterminal& temprnon=findChangeableNonterminal(tempNextSymbol);
  						if(!findSymbol(empty,temprnon.firstSet)){
    						allEmpty=false;		//empty is not in this symbol's first set,so indicator will change
    						break;
    					}
						}
						if(allEmpty){
  						vector< list<string> >::iterator vitr;
  						for(vitr=followSet.hashTable.begin();vitr!=followSet.hashTable.end();++vitr){
    						if((*vitr).size()!=0){
      						list<string>::iterator litr;
        					for(litr=(*vitr).begin();litr!=(*vitr).end();++litr){
	  								if((*litr)!=empty){
	    								if(!findSymbol(*litr,rnon.followSet))
	      								proFollowChanged=true;
	    								rnon.addToFollowSet(*litr);
	  								}
        					}
    						}
  						}
						}
					}
				}
				else{		//otherwise current nonterminal is the last symbol of production					
					vector< list<string> >::iterator vitr;
					for(vitr=followSet.hashTable.begin();vitr!=followSet.hashTable.end();++vitr){
						if((*vitr).size()!=0){
							list<string>::iterator litr;
							for(litr=(*vitr).begin();litr!=(*vitr).end();++litr){
								if(!findSymbol(*litr,rnon.followSet))		//one symbol's follow set will change
									proFollowChanged=true;
								rnon.addToFollowSet(*litr);
							}
						}
					}
				}
			}
		}while(proStr.length());
	}
}

/* Function to output first set and follow set information to file */
void Nonterminal::writeToFile(ofstream& outfile){
	outfile<<index<<".";
	outfile<<name<<" "<<endl<<endl;
	outfile<<"First set: { ";
	firstSet.output(outfile,',');
	outfile<<" }"<<endl;
	outfile<<"Follow set: { ";
	followSet.output(outfile,',');
	outfile<<" }"<<endl<<endl;
}

/* Get name */
const string& Nonterminal::getName(){
	return name;
}

/* Mutator for nonterminal's index */
void Nonterminal::setIndex(int num){
	index=num;
}

/* Mutator for nonterminal's name */
void Nonterminal::setName(string str){
	name=str;
}
	
/* Function to output production */
void Nonterminal::outProduction(){
	list<string>::iterator itr;
	for(itr=productionSet.begin();itr!=productionSet.end();++itr)
		cout<<*itr<<endl;
}

/* Function to indicate if first set changed */
bool Nonterminal::firstSetChanged(){
	return firstChanged;
}

/* Function to indicate if any symbol's follow set changed */
bool Nonterminal::proFollowSetChanged(){
	return proFollowChanged;
}

/* Function to add a symbol into first set */
void Nonterminal::addToFirstSet(const string& str){
	firstSet.insertRecord(str);
}

/* Function to add a symbol into follow set */
void Nonterminal::addToFollowSet(const string& str){
	followSet.insertRecord(str);
}

/* Function to find if a symbol already in hash map */
bool Nonterminal::findSymbol(const string& symbol,Hashmap& hm){
	return hm.findRecord(symbol);
}
