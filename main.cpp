#include <iostream>
#include <cstdlib>
#include "Nonterminal.h"
using std::cout;
using std::endl;

const int FILELEN=100;
const int LINELEN=256;		//max length of a line
const int TOTAL=100;		//how many nonterminals we have

Hashmap terminalSet;		//set of terminal
vector<Nonterminal> nonterminal(TOTAL);		//set of nonterminal

bool isTerminal(const string& symbol);		//check if a symbol is terminal
string firstSymbol(const string& production);		//get first symbol of a string
void cutFirstSymbol(string& production);		//cut first symbol of a string

void buildTerminalSet(ifstream& terminal);		//function to construct terminal set
void initializeNonterminal(ifstream& syntax);		//first pass to read syntax file and initialize all the nonterminals
void buildFirstSet();		//function to construct first set of nonterminal
void buildFollowSet();		//function to construct follow set of nonterminal
void outputInfo(ofstream& info);		//function to output nonterminal's information to file

/***      Rules        ***
 *** The grammar should be written in BNF rule,the input formation as follows:               ***
 *** index.nonterminal -> production1 | production2 | production3 | ... | productionN        ***
 *** there are two blank spaces before and after the arrow-> and the vertical bar|           ***
 *** and each symbol is separated by a blank space,there's no extra space at the end of line ***
 *** and no blank line in input                                                              ***/
int main(){
	char terminalfile[FILELEN]="G:\\terminal.txt";
	char syntaxfile[FILELEN]="G:\\syntax.txt";
	char infofile[FILELEN]="G:\\info.txt";
	
	ifstream terminal(terminalfile);
	if(!terminal){
		cout<<"terminal file open failed."<<endl;
		exit(1);
	}
	buildTerminalSet(terminal);
	terminal.close();
	
	ifstream syntax(syntaxfile);
	if(!syntax){
		cout<<"syntax file open failed."<<endl;
		exit(1);
	}
	
	ofstream info(infofile);
	if(!info){
		cout<<"infomation file open failed."<<endl;
		exit(1);
	}
	
	initializeNonterminal(syntax);
	syntax.close();
	
	buildFirstSet();
	buildFollowSet();
	outputInfo(info);
	info.close();
		
	return 0;
}

bool isTerminal(const string& symbol){
	return terminalSet.findRecord(symbol);
}

bool isLastSymbol(int beginPos,const string& symbol,const string& str){
	if(!(beginPos<str.length()))
		return true;
	int end=str.length();
	string last=str.substr(beginPos,end-beginPos);
	return symbol==last;
}

string firstSymbol(const string& production){
	string separator=" ";
	int end=production.find_first_of(separator);
	if(end==string::npos)		//if there is only one symbol in production
		end=production.length();
	return production.substr(0,end);
}

void cutFirstSymbol(string& production){
	if(production.length()==0)
		return;
	string separator=" ";
	int end=production.find_first_of(separator);
	if(end==string::npos)		//if there is only one symbol in production
		end=production.length();
	else
		++end;
	production=production.substr(end,production.length()-end);
}

/* Function to construct terminal set */
void buildTerminalSet(ifstream& terminal){
	string word;
	while(!terminal.eof()){
		terminal>>word;
		terminalSet.insertRecord(word);
	}
}

/* First pass to read syntax file and initialize all the nonterminals */
void initializeNonterminal(ifstream& syntax){
	char line[LINELEN];
	string lineStr;
	int count=0;
	string space=" ";
	string dot=".";
	string arrow="->";
	string bar="|";
	while(!syntax.eof()){
		syntax.getline(line,LINELEN);
		lineStr=line;
		++count;
		int start=lineStr.find(dot);
		++start;
		int end=lineStr.find(space,start);
		string str=lineStr.substr(start,end-start);
		nonterminal[count-1].setIndex(count);
		nonterminal[count-1].setName(str);
		start=lineStr.find(arrow)+3;
		while(end<lineStr.length()){
			start=lineStr.find_first_not_of(space,start);
			end=lineStr.find(bar,start)-1;
			if(end==string::npos)
				end=lineStr.length();
			str=lineStr.substr(start,end-start);
			nonterminal[count-1].addToProductionSet(str);
			start=end+2;
		}
	}
	nonterminal.resize(count);
}

/* Function to construct first set of nonterminal */
void buildFirstSet(){
	vector<Nonterminal>::iterator itr;
	bool firstGoOn=true;
	while(firstGoOn){
		int noChange=0;
		for(itr=nonterminal.begin();itr!=nonterminal.end();++itr){
			(*itr).constructFirst();
			if(!(*itr).firstSetChanged())
				++noChange;
		}
		if(noChange==nonterminal.size())
			firstGoOn=false;
		else
			firstGoOn=true;
	}
}

/* Function to construct follow set of nonterminal */
void buildFollowSet(){
	(*(nonterminal.begin())).initializeFollowSet();		//add EOF to start symbol's follow set
	vector<Nonterminal>::iterator itr;
	bool followGoOn=true;
	while(followGoOn){
		int noChange=0;
		for(itr=nonterminal.begin();itr!=nonterminal.end();++itr){
			(*itr).constructFollow();
			if(!(*itr).proFollowSetChanged())
				++noChange;
		}
		if(noChange==nonterminal.size())
			followGoOn=false;
		else
			followGoOn=true;
	}
}

/* Function to output nonterminal's information to file */
void outputInfo(ofstream& info){
	vector<Nonterminal>::iterator itr;
	for(itr=nonterminal.begin();itr!=nonterminal.end();++itr)
		(*itr).writeToFile(info);
}
